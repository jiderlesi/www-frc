// Author: Haoxiang Zhao

import { defineNuxtPlugin } from "nuxt/app";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import colors from "vuetify/lib/util/colors.mjs";

export default defineNuxtPlugin((nuxtApp) => {
	const vuetify = createVuetify({
		components,
		directives,
		theme: {
			defaultTheme: "hxlight",
			themes: {
				hxlight: {
					dark: false,
					colors: {
						primary: colors.green.darken2,
					},
				},
				hxdark: {
					dark: true,
					colors: {
						primary: colors.green.darken2,
					},
				},
			},
		},
	});
	nuxtApp.vueApp.use(vuetify);
});
