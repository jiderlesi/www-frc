// Author: Haoxiang Zhao

import { defineNuxtPlugin } from "nuxt/app";

let baseurl: string = "/";

let hxfetch: object = {
	get: (url: string, payload: object | any) => {
		let payloadobject = new URLSearchParams(payload);
		let params: string = payloadobject.toString();
		return fetch(baseurl.concat(url).concat("?").concat(params), {
			method: "GET",
		});
	},
	post: (url: string, payload: object) => {
		return fetch(baseurl.concat(url), {
			method: "POST",
			body: JSON.stringify(payload),
		});
	},
};

export default defineNuxtPlugin(() => {
	return {
		provide: {
			hxfetch: hxfetch,
		},
	};
});
