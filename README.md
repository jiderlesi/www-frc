## Info

This is the vue source code for frcsm.de

## Setup

Make sure to install the dependencies:

```bash
# nodejs
sudo apt install -y nodejs npm

# npm
npm install
```

## Generate webpage

```bash
npm run generate
```

## Copy the webpage to www-root

