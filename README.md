## Info

This repo is forked from Block81838(Haoxiang Zhao)'s website.
I replaced some configuration for my website.
In other respects I replaced the proprietary icons in the original repository with free icons. If there is other proprietary content in future updates I will also try to replace it with a freedom alternative.

## Setup

Make sure to install the dependencies:

```bash
# nodejs
sudo apt install -y nodejs npm

# npm
npm install
```

## Generate webpage

```bash
npm run generate
```

## Copy the webpage to www-root

